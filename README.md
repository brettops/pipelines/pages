# pages pipeline

<!-- BADGIE TIME -->

[![brettops pipeline](https://img.shields.io/badge/brettops-pipeline-209cdf?labelColor=162d50)](https://brettops.io)
[![pipeline status](https://gitlab.com/brettops/pipelines/pages/badges/main/pipeline.svg)](https://gitlab.com/brettops/pipelines/pages/-/commits/main)
[![pre-commit](https://img.shields.io/badge/pre--commit-enabled-brightgreen?logo=pre-commit)](https://github.com/pre-commit/pre-commit)
[![code style: prettier](https://img.shields.io/badge/code_style-prettier-ff69b4.svg)](https://github.com/prettier/prettier)

<!-- END BADGIE TIME -->

The `pages` pipeline publishes the contents of a `public` directory to [GitLab
Pages](https://docs.gitlab.com/ee/user/project/pages/#how-it-works) on the
default branch. This ensures that documentation is always being built, but only
published when desired.

View the example site: https://brettops.gitlab.io/pipelines/pages/

## Usage

Include the pipeline in your `.gitlab-ci.yml` file:

```yaml
include:
  - project: brettops/pipelines/pages
    file: include.yml
```

## Examples

### Publish a MkDocs site

Add a `mkdocs.yml` config and a `docs` folder to the root of your project.

Then, add the following to your `.gitlab-ci.yml` file:

```yaml
include:
  - project: brettops/pipelines/mkdocs
    file: include.yml
  - project: brettops/pipelines/pages
    file: include.yml
```

### Publish anything

You can publish anything you like by placing it in the `public` directory and
saving as an artifact:

```yaml
stages:
  - test
  - build
  - deploy

include:
  - project: brettops/pipelines/pages
    file: include.yml

totally-custom-job:
  stage: build
  image: ${CONTAINER_PROXY}alpine
  script:
    - mkdir public
    - touch public/index.html
  artifacts:
    paths:
      - public/
```
